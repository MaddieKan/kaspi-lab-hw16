package kaspi.services;

import kaspi.dao.OrderDao;
import kaspi.dao.Dao;
import kaspi.entities.Order;

import java.util.List;

public class OrderService {
    private static Dao<Order, Integer> orderDao;

    public OrderService() {
        orderDao = new OrderDao();
    }

    public void persist(Order order) {
        orderDao.create(order);
    }

    public Order retrieve(Integer id) {
        return orderDao.retrieve(id);
    }

    public List<Order> retrieveAll() {
        return orderDao.retrieveAll();
    }

    public void update(Order order) {
        orderDao.update(order);
    }

    public void delete(Order order) {
        orderDao.delete(order);
    }
}
