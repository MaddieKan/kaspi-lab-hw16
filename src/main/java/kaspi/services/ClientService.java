package kaspi.services;

import kaspi.dao.ClientDao;
import kaspi.dao.Dao;
import kaspi.entities.Client;

import java.util.List;

public class ClientService {
    private static Dao<Client, Integer> clientDao;

    public ClientService() {
        clientDao = new ClientDao();
    }

    public void persist(Client client) {
        clientDao.create(client);
    }

    public Client retrieve(Integer id) {
        return clientDao.retrieve(id);
    }

    public List<Client> retrieveAll() {
        return clientDao.retrieveAll();
    }

    public void update(Client client) {
        clientDao.update(client);
    }

    public void delete(Client client) {
        clientDao.delete(client);
    }
}
