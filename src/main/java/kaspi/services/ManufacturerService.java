package kaspi.services;

import kaspi.dao.Dao;
import kaspi.dao.ManufacturerDao;
import kaspi.entities.Manufacturer;

import java.util.List;

public class ManufacturerService {
    private static Dao<Manufacturer, Integer> manufacturerDao;

    public ManufacturerService() {
        manufacturerDao = new ManufacturerDao();
    }

    public void persist(Manufacturer manufacturer) {
        manufacturerDao.create(manufacturer);
    }

    public Manufacturer retrieve(Integer id) {
        return manufacturerDao.retrieve(id);
    }

    public List<Manufacturer> retrieveAll() {
        return manufacturerDao.retrieveAll();
    }

    public void update(Manufacturer manufacturer) {
        manufacturerDao.update(manufacturer);
    }

    public void delete(Manufacturer manufacturer) {
        manufacturerDao.delete(manufacturer);
    }
}
