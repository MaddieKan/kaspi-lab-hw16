package kaspi.services;

import kaspi.dao.ProductDao;
import kaspi.entities.Product;

import java.util.List;

public class ProductService {
    private static ProductDao productDao;

    public ProductService() {
        productDao = new ProductDao();
    }

    public void persist(Product product) {
        productDao.create(product);
    }

    public Product retrieve(Integer id) {
        return productDao.retrieve(id);
    }

    public List<Product> retrieveAll() {
        return productDao.retrieveAll();
    }

    public void update(Product product) {
        productDao.update(product);
    }

    public void delete(Product product) {
        productDao.delete(product);
    }

    public List<Product> getPopularProducts() {
        return productDao.getPopularProducts();
    }
}
