package kaspi.dao;

import kaspi.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;

public abstract class BaseDao<T extends Serializable, ID
        extends Serializable> implements Dao<T, ID> {
    private Class<T> entityClass;
    private Session session;

    public BaseDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public Session openSession() {
        session = HibernateUtil.obtainSessionFactory().openSession();
        return session;
    }

    @Override
    public void create(T entity) {
        openSession();
        executeInsideTransaction(session -> session.save(entity));
    }

    @Override
    public T retrieve(ID id) {
        T entity = openSession().get(entityClass, id);
        session.close();
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> retrieveAll() {
        List<T> entities = openSession().createQuery(
                "FROM " + entityClass.getName()).list();
        session.close();
        return entities;
    }

    @Override
    public void update(T entity) {
        openSession();
        executeInsideTransaction(session -> session.update(entity));
    }

    @Override
    public void delete(T entity) {
        openSession();
        executeInsideTransaction(session -> session.delete(entity));
    }

    private void executeInsideTransaction(Consumer<Session> command) {
        Transaction transaction = session.beginTransaction();
        command.accept(session);
        transaction.commit();
        session.close();
    }
}
