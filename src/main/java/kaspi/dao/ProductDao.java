package kaspi.dao;

import kaspi.entities.Product;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.List;

public class ProductDao extends BaseDao<Product, Integer> implements ProductDaoInterface {

    public ProductDao() {
        super(Product.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> getPopularProducts() {
        Session session = openSession();
        Query query = session.createQuery(
                "SELECT p from Product p LEFT JOIN " +
                        "p.orders o GROUP BY p ORDER BY count(o) DESC");
        query.setMaxResults(2);
        List<Product> products = query.getResultList();
        session.close();
        return products;
    }
}
