package kaspi.dao;

import kaspi.entities.Product;

import java.util.List;

public interface ProductDaoInterface {

    public List<Product> getPopularProducts();
}
