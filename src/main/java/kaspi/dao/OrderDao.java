package kaspi.dao;

import kaspi.entities.Order;

public class OrderDao extends BaseDao<Order, Integer> implements OrderDaoInterface {

    public OrderDao() {
        super(Order.class);
    }
}
