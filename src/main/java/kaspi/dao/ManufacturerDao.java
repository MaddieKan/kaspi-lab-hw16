package kaspi.dao;

import kaspi.entities.Manufacturer;

public class ManufacturerDao extends BaseDao<Manufacturer, Integer> implements ManufacturerDaoInterface {

    public ManufacturerDao() {
        super(Manufacturer.class);
    }
}
