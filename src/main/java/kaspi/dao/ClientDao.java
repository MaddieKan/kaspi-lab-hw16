package kaspi.dao;

import kaspi.entities.Client;

public class ClientDao extends BaseDao<Client, Integer> implements ClientDaoInterface {

    public ClientDao() {
        super(Client.class);
    }
}
