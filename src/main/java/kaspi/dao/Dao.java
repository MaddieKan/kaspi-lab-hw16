package kaspi.dao;

import org.hibernate.Session;
import java.io.Serializable;
import java.util.List;

public interface Dao<T extends Serializable, ID extends Serializable> {

    public Session openSession();

    public void create(T entity);

    public T retrieve(ID id);

    public List<T> retrieveAll();

    public void update(T entity);

    public void delete(T entity);
}
