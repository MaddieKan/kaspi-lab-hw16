package kaspi;

import kaspi.services.*;
import kaspi.entities.*;
import kaspi.utils.HibernateUtil;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    static ClientService clientService = new ClientService();
    static OrderService orderService = new OrderService();
    static ManufacturerService manufacturerService = new ManufacturerService();
    static ProductService productService = new ProductService();

    public static void main(String[] args) {
        clientCRUD();
        manufacturerCRUD();
        productCRUD();
        orderCRUD();
        HibernateUtil.closeSessionFactory();
    }

    public static void clientCRUD() {
        clientService.persist(new Client("Madina", "Balkhash, Kazbekova st. 55"));
        clientService.persist(new Client("Albert", "Almaty, Gogol st. 140"));
        clientService.persist(new Client("Alice", "Almaty, Minina st. 24"));

        clientService.retrieveAll().forEach(System.out::println);

        Client client = clientService.retrieve(3);
        client.setName("Alissa");
        clientService.update(client);

        clientService.delete(client);
    }

    public static void manufacturerCRUD() {
        manufacturerService.persist(new Manufacturer("Asus", "https://www.asus.com/"));
        manufacturerService.persist(new Manufacturer("Apple", "https://www.apple.com/"));

        manufacturerService.retrieveAll().forEach(System.out::println);

        Manufacturer manufacturer = manufacturerService.retrieve(1);
        manufacturer.setSiteLink("https://www.asus.com/kz/");
        manufacturerService.update(manufacturer);

        manufacturerService.delete(manufacturer);
    }

    public static void productCRUD() {
        productService.persist(new Product("MacBook Pro M1", 1800, manufacturerService.retrieve(2)));
        productService.persist(new Product("MacBook Air M1", 1300, manufacturerService.retrieve(2)));
        productService.persist(new Product("MacBook Air Intel", 1000, manufacturerService.retrieve(2)));

        productService.retrieveAll().forEach(System.out::println);

        Product product = productService.retrieve(3);
        product.setPrice(900);
        productService.update(product);

        productService.delete(product);
    }

    public static void orderCRUD() {
        orderService.persist(new Order(clientService.retrieve(1), Stream.of(
                productService.retrieve(1),
                productService.retrieve(2)
        ).collect(Collectors.toSet())));
        orderService.persist(new Order(clientService.retrieve(2), Stream.of(
                productService.retrieve(2)
        ).collect(Collectors.toSet())));
        orderService.persist(new Order(clientService.retrieve(1)));

        orderService.retrieveAll().forEach(System.out::println);

        Order order = orderService.retrieve(1);
//        Set<Product> products = order.getProducts();
//        products.remove(products.iterator().next());
//        order.setProducts(products);
        order.addProducts(productService.retrieve(2));
        orderService.update(order);

        orderService.delete(order);

        productService.getPopularProducts().forEach(System.out::println);
    }
}
