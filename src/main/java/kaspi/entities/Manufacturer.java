package kaspi.entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "manufacturer")
public class Manufacturer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "manufacturer_id_seq",
            sequenceName = "manufacturer_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "manufacturer_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "site_link")
    private String siteLink;

    public Manufacturer() { }

    public Manufacturer(String name, String siteLink) {
        this.name = name;
        this.siteLink = siteLink;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSiteLink() {
        return siteLink;
    }

    public void setSiteLink(String siteLink) {
        this.siteLink = siteLink;
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", siteLink='" + siteLink + '\'' +
                '}';
    }
}
