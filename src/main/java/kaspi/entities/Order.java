package kaspi.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "shop_order")
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "shop_order_id_seq",
            sequenceName = "shop_order_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "shop_order_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @ManyToOne(cascade = {CascadeType.PERSIST,
            CascadeType.REFRESH},
            optional = false)
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "order_time")
    private LocalDateTime orderTime;

    @ManyToMany(cascade = CascadeType.PERSIST,
            fetch = FetchType.EAGER)
    @JoinTable(name = "order_product",
            joinColumns = { @JoinColumn(name = "order_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") })
    private Set<Product> products;

    public Order() { }

    public Order(Client client) {
        this.client = client;
        this.orderTime = LocalDateTime.now();
        this.products = new HashSet<>();
    }

    public Order(Client client, Set<Product> products) {
        this.client = client;
        this.orderTime = LocalDateTime.now();
        this.products = products;
    }

    public Integer getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public void addProducts(Product product) {
        products.add(product);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", client=" + client +
                ", orderTime=" + orderTime +
                ", products=" + products +
                '}';
    }
}
