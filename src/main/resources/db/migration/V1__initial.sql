CREATE TABLE client (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    address VARCHAR(100)
);

CREATE TABLE shop_order (
    id SERIAL PRIMARY KEY,
    client_id INTEGER REFERENCES client (id) ON DELETE RESTRICT NOT NULL,
    order_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE manufacturer (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    site_link VARCHAR(100)
);

CREATE TABLE product (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    price INTEGER NOT NULL,
    manufacturer_id INTEGER REFERENCES manufacturer (id) ON DELETE RESTRICT NOT NULL
);

CREATE TABLE order_product (
    order_id INTEGER REFERENCES shop_order (id) ON DELETE CASCADE,
    product_id INTEGER REFERENCES product (id) ON DELETE CASCADE,
    PRIMARY KEY (order_id, product_id)
);